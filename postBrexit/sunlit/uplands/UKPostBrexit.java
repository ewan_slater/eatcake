package sunlit.uplands;

import eu.singlemarket.*;
import eu.singlemarket.thirdcountry.access.*;

public class UKPostBrexit {
	public void sellGoodsToMemberOfEU(MemberAccessedExternally mae) {
		mae.acceptForeignGoods(new ForeignGoods("Chlorinated Mad Cow"));
	}
	
	public void sellFinancialServicesToMemberOfEU(Member m) {
		m.acceptServices(new Services("Latest Ponzi Scheme"));
	}
	
	public void sellFinancialServicesToMemberOfEU(MemberAccessedExternally mae) {
		mae.acceptForeignServices(new FinServ("Chlorinated Britcoin"));
	}
}
